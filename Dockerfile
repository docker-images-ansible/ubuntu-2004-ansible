FROM ubuntu:20.04

RUN ln -snf /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime && echo $CONTAINER_TIMEZONE > /etc/timezone
RUN apt-get update && apt-get -y install build-essential libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev wget ca-certificates curl snapd python3-pip
RUN apt install -y openssh-server
RUN systemctl enable ssh

# Install Git and Ansible
RUN apt install -y git
RUN apt install -y ansible
RUN ansible-galaxy collection install community.general
COPY ./requirements.yml /
RUN ansible-galaxy install -r requirements.yml

# Install kubectl from Docker Hub.
#COPY --from=lachlanevenson/k8s-kubectl:v1.10.3 /usr/local/bin/kubectl /usr/local/bin/kubectl
#RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
#RUN chmod +x ./kubectl
#RUN mv ./kubectl /usr/local/bin/kubectl

# Install Helm
#RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
#RUN chmod +x get_helm.sh && ./get_helm.sh

# Install Docker
RUN pip3 install docker
RUN pip3 install docker-py